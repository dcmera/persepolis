package co.edu.uniandes.km.entrega3.perfilamiento;

import java.util.ArrayList;
import java.util.List;

public class Vertical {
	
	private String nombre;
	private List<String> enfoquesPlanDesarrollo;
	private List<String> areasPrioritarias;
	
	public Vertical() { 
		this.nombre = "";
		this.enfoquesPlanDesarrollo = new ArrayList<>();
		this.areasPrioritarias = new ArrayList<>();
	}

	public Vertical(String nombre, List<String> enfoques,  List<String> areasPrioritarias) {
		this.nombre = nombre;
		this.enfoquesPlanDesarrollo = enfoques;
		this.areasPrioritarias = areasPrioritarias;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the areasPrioritarias
	 */
	public List<String> getAreasPrioritarias() {
		return areasPrioritarias;
	}

	/**
	 * @param areasPrioritarias the areasPrioritarias to set
	 */
	public void setAreasPrioritarias(List<String> areasPrioritarias) {
		this.areasPrioritarias = areasPrioritarias;
	}
	
	/**
	 * @return the enfoquesPlanDesarrollo
	 */
	public List<String> getEnfoquesPlanDesarrollo() {
		return enfoquesPlanDesarrollo;
	}

	/**
	 * @param enfoquesPlanDesarrollo the enfoquesPlanDesarrollo to set
	 */
	public void setEnfoquesPlanDesarrollo(List<String> enfoquesPlanDesarrollo) {
		this.enfoquesPlanDesarrollo = enfoquesPlanDesarrollo;
	}

	public boolean agregarArea(String area){
		return this.areasPrioritarias.add(area);
	}
	
	public boolean removerArea(String area){
		return this.areasPrioritarias.remove(area);
	}
}