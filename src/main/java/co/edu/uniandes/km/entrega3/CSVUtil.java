package co.edu.uniandes.km.entrega3;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import co.edu.uniandes.km.entrega3.bancoproyectos.*;
import co.edu.uniandes.km.entrega3.perfilamiento.*;

public final class CSVUtil {
	
	private CSVUtil() { }
	
	private static List<CSVRecord> obtenerFilas(String rutaArchivo, String[] cabeceraArchivo) throws IOException{
		List<CSVRecord> records = new ArrayList<>();
		Reader lector = new FileReader(rutaArchivo);
		CSVFormat formato = CSVFormat.EXCEL.withHeader(cabeceraArchivo).withDelimiter(',');
		CSVParser parser = new CSVParser(lector, formato);
		records.addAll(parser.getRecords());
		parser.close();
		records.remove(0);
		return records;
	}
	
	private static Double stringToDouble(String nombreColumna, CSVRecord fila){
		String texto = fila.get(nombreColumna).replaceAll("\\$", "").replaceAll(",", "");
		if(StringUtils.isBlank(texto))
			return 0d;
		else
			return Double.parseDouble(texto);
	}

	public static Municipio obtenerMunicipio(String rutaArchivoMunicipio) throws IOException, IllegalArgumentException{
		String[] cabecera = {"Municipio", "Clase", "Presupuesto", "I_eficacia",
				"I_cumplimiento", "I_desarrollo", "Periodo vigente"};
		List<CSVRecord> filas = obtenerFilas(rutaArchivoMunicipio, cabecera);
		if(!filas.isEmpty()){
			CSVRecord datosMunicipio = filas.get(0);
			String nombre = datosMunicipio.get("Municipio");
			ClaseMunicipio clase = ClaseMunicipio.fromInteger(Integer.parseInt(datosMunicipio.get("Clase")));
			Double presupuesto = stringToDouble("Presupuesto", datosMunicipio);
			Double indicadorEficiencia = Double.parseDouble(datosMunicipio.get("I_eficacia"));
			Double indicadorCumplimiento = Double.parseDouble(datosMunicipio.get("I_cumplimiento"));
			Double indicadorDesarrollo = Double.parseDouble(datosMunicipio.get("I_desarrollo"));
			Double aniosRestantesAlcande = Double.parseDouble(datosMunicipio.get("Periodo vigente"));
			List<EnfoquePlanDesarrollo> planDesarrollo = new ArrayList<>();
			return new Municipio(nombre, clase, presupuesto, indicadorEficiencia, indicadorCumplimiento,
					indicadorDesarrollo, aniosRestantesAlcande, planDesarrollo);
		}
		else
			throw new IllegalArgumentException("El archivo con la informacion del municipio no puede estar vacio");
	}
	
	public static List<EnfoquePlanDesarrollo> obtenerPlanDesarrollo(String rutaPlanDesarrollo) throws IOException, IllegalArgumentException{
		String[] cabecera = {"Id", "Vertical", "Tipo"};
		List<CSVRecord> filas = obtenerFilas(rutaPlanDesarrollo, cabecera);
		if(!filas.isEmpty()){
			List<EnfoquePlanDesarrollo> planDesarrollo = new ArrayList<>();
			List<Vertical> verticales = new ArrayList<>();			filas.forEach(fila -> {
				String name = fila.get("Id");
				String tipo = fila.get("Tipo");				
				EnfoquePlanDesarrollo enfoque = new EnfoquePlanDesarrollo(name, verticales, tipo);
				planDesarrollo.add(enfoque);
			});
			return planDesarrollo;
		}
		else
			throw new IllegalArgumentException("El archivo con la informacion del plan de desarrollo del municipio no puede estar vacio");
	}
	
	public static List<Vertical> obtenerVerticales(String rutaVerticales) throws IOException, IllegalArgumentException{
		String[] cabecera = {"Nombre", "PdD", "Area prioritaria"};
		List<CSVRecord> filas = obtenerFilas(rutaVerticales, cabecera);
		if(!filas.isEmpty()){
			List<Vertical> verticales = new ArrayList<>();
			filas.forEach(fila -> {
				String nombre = fila.get("Nombre");
				List<String> areasPrioritarias = Arrays.asList(fila.get("Area prioritaria").split(","));
				List<String> enfoques = Arrays.asList(fila.get("PdD").split(","));
				Vertical vertical = new Vertical(nombre, enfoques, areasPrioritarias);
				verticales.add(vertical);
			});
			return verticales;
		}
		else
			throw new IllegalArgumentException("El archivo con la informacion de verticales no puede estar vacio");
	}
	
	public static List<Iniciativa> obtenerIniciativas(String rutaIniciativas) throws IOException, IllegalArgumentException{
		String[] cabecera = {"Nombre", "Solucion", "Area prioritaria"};
		List<CSVRecord> filas = obtenerFilas(rutaIniciativas, cabecera);
		if(!filas.isEmpty()){
			List<Iniciativa> iniciativas = new ArrayList<>();
			filas.forEach(fila -> {
				String nombre = fila.get("Nombre");
				List<String> areaPrioritaria = Arrays.asList(fila.get("Area prioritaria").split(","));
				List<Solucion> soluciones = new ArrayList<>();
				Iniciativa iniciativa = new Iniciativa(nombre, areaPrioritaria, soluciones);
				iniciativas.add(iniciativa);
			});
			return iniciativas;
		}
		else
			throw new IllegalArgumentException("El archivo con la informacion de las iniciativas no puede estar vacio");
	}
	
	public static List<Solucion> obtenerSoluciones(String rutaSoluciones) throws IOException, IllegalArgumentException{
		String[] cabecera = {"Nombre", "Costo", "Tiempo", "I_eficiencia", "I_cumplimiento", "I_desarrollo", "Iniciativas"};
		List<CSVRecord> filas = obtenerFilas(rutaSoluciones, cabecera);
		if(!filas.isEmpty()){
			List<Solucion> soluciones = new ArrayList<>();
			filas.forEach(fila -> {
				String nombre = fila.get("Nombre");
				String iniciativa = fila.get("Iniciativas");
				Double costo = stringToDouble("Costo", fila);
				Double tiempoEjecucion = stringToDouble("Tiempo", fila);
				Double indicadorEficiencia = stringToDouble("I_eficiencia", fila);
				Double indicadorCumplimiento = stringToDouble("I_cumplimiento", fila);
				Double indicadorDesarrollo = stringToDouble("I_desarrollo", fila);
				List<ServicioNegocio> servicios = new ArrayList<>();
				Solucion solucion = new Solucion(nombre, iniciativa, costo, tiempoEjecucion,
						indicadorEficiencia, indicadorCumplimiento, indicadorDesarrollo, servicios);
				soluciones.add(solucion);
			});
			return soluciones;
		}
		else
			throw new IllegalArgumentException("El archivo con la informacion de soluciones no puede estar vacio");
	}
	
	public static List<ServicioNegocio> obtenerServiciosNegocio(String rutaServicios) throws IOException, IllegalArgumentException{
		String[] cabecera = {"Nombre", "Solucion", "Aplicacion"};
		List<CSVRecord> filas = obtenerFilas(rutaServicios, cabecera);
		if(!filas.isEmpty()){
			List<ServicioNegocio> servicios = new ArrayList<>();
			filas.forEach(fila ->{
				String nombre = fila.get("Nombre");
				String solucion = fila.get("Solucion");
				List<String> aplicaciones = Arrays.asList(fila.get("Aplicacion").split(","));
				ServicioNegocio servicio = new ServicioNegocio(nombre, solucion, aplicaciones);
				servicios.add(servicio);
			});
			return servicios;
		}
		else
			throw new IllegalArgumentException("El archivo con la informcion de los servicios de negocio no puede estar vacio");
	}

}
