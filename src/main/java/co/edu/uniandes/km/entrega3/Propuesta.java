package co.edu.uniandes.km.entrega3;

import java.util.List;

import co.edu.uniandes.km.entrega3.bancoproyectos.Solucion;

public class Propuesta {

	private List<Solucion> proyectos;
	
	public Propuesta(List<Solucion> proyectos){
		this.proyectos = proyectos;
	}

	/**
	 * @return the proyectos
	 */
	public List<Solucion> getProyectos() {
		return proyectos;
	}

	/**
	 * @param proyectos the proyectos to set
	 */
	public void setProyectos(List<Solucion> proyectos) {
		this.proyectos = proyectos;
	}
	
	public boolean agregarProyecto(Solucion proyecto){
		return this.proyectos.add(proyecto);
	}
	
	public boolean removerProyecto(Solucion proyecto){
		return this.proyectos.remove(proyecto);
	}
}