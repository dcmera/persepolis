package co.edu.uniandes.km.entrega3;

public class Validacion {

	private Estado estado;

	public Validacion(Estado estado) {
		this.estado = estado;
	}

	/**
	 * @return the estado
	 */
	public Estado getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
}