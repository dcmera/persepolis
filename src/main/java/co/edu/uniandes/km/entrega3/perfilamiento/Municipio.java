package co.edu.uniandes.km.entrega3.perfilamiento;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Municipio {
	
	private String nombre;
	private ClaseMunicipio clase;
	private Double presupuesto;
	private Double indicadorEficiencia;
	private Double indicadorCumplimiento;
	private Double indicadorDesarrollo;
	private Double aniosRestantesAlcalde;
	private List<EnfoquePlanDesarrollo> planDesarrollo;
	
	public Municipio() { 
		this.nombre = "";
		this.clase = ClaseMunicipio.UNO;
		this.presupuesto = 0d;
		this.indicadorEficiencia = 0d;
		this.indicadorCumplimiento = 0d;
		this.indicadorDesarrollo = 0d;
		this.aniosRestantesAlcalde = 0d;
		this.planDesarrollo = new ArrayList<>();
	}

	public Municipio(String nombre, ClaseMunicipio clase, Double presupuesto, Double indicadorEficiencia,
			Double indicadorCumplimiento, Double indicadorDesarrollo, Double aniosRestantesAlcande,
			List<EnfoquePlanDesarrollo> planDesarrollo) {
		this.nombre = nombre;
		this.clase = clase;
		this.presupuesto = presupuesto;
		this.indicadorEficiencia = indicadorEficiencia;
		this.indicadorCumplimiento = indicadorCumplimiento;
		this.indicadorDesarrollo = indicadorDesarrollo;
		this.aniosRestantesAlcalde = aniosRestantesAlcande;
		this.planDesarrollo = planDesarrollo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the clase
	 */
	public ClaseMunicipio getClase() {
		return clase;
	}

	/**
	 * @param clase the clase to set
	 */
	public void setClase(ClaseMunicipio clase) {
		this.clase = clase;
	}

	/**
	 * @return the presupuesto
	 */
	public Double getPresupuesto() {
		return presupuesto;
	}

	/**
	 * @param presupuesto the presupuesto to set
	 */
	public void setPresupuesto(Double presupuesto) {
		this.presupuesto = presupuesto;
	}

	/**
	 * @return the indicadorEficiencia
	 */
	public Double getIndicadorEficiencia() {
		return indicadorEficiencia;
	}

	/**
	 * @param indicadorEficiencia the indicadorEficiencia to set
	 */
	public void setIndicadorEficiencia(Double indicadorEficiencia) {
		this.indicadorEficiencia = indicadorEficiencia;
	}

	/**
	 * @return the indicadorCumplimiento
	 */
	public Double getIndicadorCumplimiento() {
		return indicadorCumplimiento;
	}

	/**
	 * @param indicadorCumplimiento the indicadorCumplimiento to set
	 */
	public void setIndicadorCumplimiento(Double indicadorCumplimiento) {
		this.indicadorCumplimiento = indicadorCumplimiento;
	}

	/**
	 * @return the indicadorDesarrollo
	 */
	public Double getIndicadorDesarrollo() {
		return indicadorDesarrollo;
	}

	/**
	 * @param indicadorDesarrollo the indicadorDesarrollo to set
	 */
	public void setIndicadorDesarrollo(Double indicadorDesarrollo) {
		this.indicadorDesarrollo = indicadorDesarrollo;
	}

	/**
	 * @return the aniosRestantesAlcande
	 */
	public Double getAniosRestantesAlcande() {
		return aniosRestantesAlcalde;
	}

	/**
	 * @param aniosRestantesAlcande the aniosRestantesAlcande to set
	 */
	public void setAniosRestantesAlcande(Double aniosRestantesAlcande) {
		this.aniosRestantesAlcalde = aniosRestantesAlcande;
	}

	/**
	 * @return the planDesarrollo
	 */
	public List<EnfoquePlanDesarrollo> getPlanDesarrollo() {
		return planDesarrollo;
	}

	/**
	 * @param planDesarrollo the planDesarrollo to set
	 */
	public void setPlanDesarrollo(List<EnfoquePlanDesarrollo> planDesarrollo) {
		this.planDesarrollo = planDesarrollo;
	}
	
	public Map<String, Map<String, List<String>>> obtenerAreasPrioritariasMunicipio(){
		final Map<String, Map<String, List<String>>> areasMunicipio = new HashMap<>();
		planDesarrollo.forEach(enfoque -> {
			final Map<String, Map<String, List<String>>> verticalesYEnfoquePorArea = obtenerEnfoquesYVerticalesPorArea(enfoque);
			verticalesYEnfoquePorArea.forEach((k, v) -> {
				if(areasMunicipio.containsKey(k)){
					List<String> verticales =  areasMunicipio.get(k).get("Vertical");
					if(verticales == null){
						verticales = new ArrayList<>();
					}
					List<String> nuevasVerticales = v.get("Vertical");
					if(nuevasVerticales == null){
						nuevasVerticales = new ArrayList<>();
					}
					verticales.addAll(nuevasVerticales);
					areasMunicipio.get(k).put("Vertical", verticales);
					List<String> enfoques = areasMunicipio.get(k).get("Enfoque");
					if(enfoques == null){
						enfoques = Collections.emptyList();
					}
					List<String> nuevosEnfoques = v.get("Enfoque");
					if(nuevosEnfoques == null){
						nuevosEnfoques = new ArrayList<>();
					}
					enfoques.addAll(nuevosEnfoques);
					areasMunicipio.get(k).put("Enfoque", nuevosEnfoques);
				}
				else{
					areasMunicipio.put(k, v);
				}
			});
		});
		return areasMunicipio;
	}
	
	private Map<String, Map<String, List<String>>> obtenerVerticalesPorArea(List<Vertical> verticales){
		
		final Map<String, Map<String, List<String>>> verticalesPorAreas = new HashMap<>();
		
		verticales.forEach(vertical -> {
			List<String> areas = vertical.getAreasPrioritarias();
			
			for(String area: areas){
				if(verticalesPorAreas.containsKey(area)){
					List<String> entryVerticales = verticalesPorAreas.get(area).get("Vertical");
					entryVerticales.add(vertical.getNombre());
					verticalesPorAreas.get(area).put("Vertical", entryVerticales);
				}
				else{
					Map<String, List<String>> entryVertical = new HashMap<>();
					List<String> vs = new ArrayList<>();
					vs.add(vertical.getNombre());
					entryVertical.put("Vertical", vs);
					verticalesPorAreas.put(area, entryVertical);
				}
			}
			
		});
		return verticalesPorAreas;
	}
	
	private Map<String, Map<String, List<String>>> obtenerEnfoquesYVerticalesPorArea(EnfoquePlanDesarrollo  enfoque){
		
		final List<Vertical> verticalesEnfoque = enfoque.getVerticales();
		
		final Map<String, Map<String, List<String>>> verticalesYEnfoquesPorArea = obtenerVerticalesPorArea(verticalesEnfoque);
		
		for(Map.Entry<String, Map<String, List<String>>> entry: verticalesYEnfoquesPorArea.entrySet()){
			Map<String, List<String>> enfoquePorArea = entry.getValue();
			List<String> es = new ArrayList<>();
			es.add(enfoque.getName());
			enfoquePorArea.put("Enfoque", es);
		}
		return verticalesYEnfoquesPorArea;
		
	}
}