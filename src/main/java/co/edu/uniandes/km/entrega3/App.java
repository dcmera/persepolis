package co.edu.uniandes.km.entrega3;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.edu.uniandes.km.entrega3.bancoproyectos.*;
import co.edu.uniandes.km.entrega3.perfilamiento.*;
import co.edu.uniandes.km.entrega3.JsonUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class App {

	private static final Logger LOG = LoggerFactory.getLogger(App.class);
		
	public static void main(String[] args) {
		try{
			System.out.println("Esta aplicacion permite"
					+ " determinar que proyectos permiten armar una propuesta para un municipio");
			KieServices kService = KieServices.Factory.get();
			KieContainer kContainer = kService.getKieClasspathContainer();
			KieSession kSession = kContainer.newKieSession("ksession-rules");
			agregarASesion(kSession);
			kSession.fireAllRules();
			kSession.dispose();
		}
		catch(Exception e){
			LOG.error("Ocurrio un error al cargar la informacion en la base de hechos", e);
		}		
	}
	
	private static void agregarASesion(KieSession kSession) throws IllegalArgumentException, IOException{
		Scanner scanner = new Scanner(System.in);
		cargarPerfilMunicipio(kSession, scanner);
		cargarBancoProyectos(kSession, scanner);
		Validacion validacion = new Validacion(Estado.SIN_VALIDACION);
		List<Solucion> proyectosPropuesta = new ArrayList<>();
		Propuesta propuesta = new Propuesta(proyectosPropuesta);
		kSession.insert(propuesta);
		kSession.insert(validacion);
	}

	private static void cargarBancoProyectos(KieSession kSession, Scanner scanner) throws IllegalArgumentException, IOException {
		System.out.println("Proporcione a continuacion las rutas de los archivos con la informacion del banco de proyectos");
		
		System.out.print("Datos de iniciativas: ");
		String rutaIniciativas = scanner.nextLine();
		//String rutaIniciativas = "D:/Documents/Desktop/csv/iniciativas.csv";
		List<Iniciativa> iniciativas = CSVUtil.obtenerIniciativas(rutaIniciativas);
		
		System.out.print("Datos de las soluciones en cada iniciativa: ");
		String rutaSoluciones = scanner.nextLine();
		//String rutaSoluciones = "D:/Documents/Desktop/csv/soluciones.csv";
		List<Solucion> soluciones = CSVUtil.obtenerSoluciones(rutaSoluciones);
		
		System.out.print("Datos de los servicios de negocio en cada solucion: ");
		String rutaServicios = scanner.nextLine();
		//String rutaServicios = "D:/Documents/Desktop/csv/servicios.csv";
		List<ServicioNegocio> servicios = CSVUtil.obtenerServiciosNegocio(rutaServicios);
		
		soluciones.forEach(solucion -> {
			final String nombreSolucion = solucion.getNombre();
			List<ServicioNegocio> serviciosSolucion = servicios.stream()
					.filter(s -> s.getSolucion().equals(nombreSolucion)).collect(Collectors.toList());
			solucion.setServiciosNegocio(serviciosSolucion);
		});
		
		iniciativas.forEach(iniciativa -> {
			String nombreIniciativa = iniciativa.getNombre();
			List<Solucion> solucionesIniciativa = soluciones.stream()
					.filter(s -> s.getIniciativa().equals(nombreIniciativa)).collect(Collectors.toList());
			iniciativa.setSoluciones(solucionesIniciativa);
		});
		BancoProyectos banco = new BancoProyectos(iniciativas);
		kSession.insert(banco);
		LOG.info("Banco de Proyetos: \n{}", JsonUtil.toJsonString(banco));
	}

	private static void cargarPerfilMunicipio(KieSession kSession, Scanner scanner) throws IllegalArgumentException, IOException {
		System.out.println("Proporcione a continuación las rutas de los archivos con la siguiente información: ");
		
		System.out.print("Datos el municipio: ");
		String rutaMunicipio = scanner.nextLine();
		//String rutaMunicipio = "D:/Documents/Desktop/csv/municipios.csv";
		Municipio municipio = CSVUtil.obtenerMunicipio(rutaMunicipio);
		
		System.out.print("Enfoques del plan del desarrollo del municipio: ");
		String rutaEnfoquesPdD = scanner.nextLine();
		//String rutaEnfoquesPdD = "D:/Documents/Desktop/csv/enfoques.csv";
		List<EnfoquePlanDesarrollo> planDesarrollo = CSVUtil.obtenerPlanDesarrollo(rutaEnfoquesPdD);
		
		System.out.print("Verticales del plan de desarrollo del municipio: ");
		String rutaVerticales = scanner.nextLine();
		//String rutaVerticales = "D:/Documents/Desktop/csv/verticales.csv";
		List<Vertical> verticales = CSVUtil.obtenerVerticales(rutaVerticales);		
		
		planDesarrollo.forEach(enfoque -> {
			String nombreEnfoque = enfoque.getName();
			List<Vertical> verticalesEnfoque = verticales
					.stream()
					.filter(v -> v.getEnfoquesPlanDesarrollo().contains(nombreEnfoque))
					.collect(Collectors.toList());
			enfoque.setVerticales(verticalesEnfoque);
		});
		municipio.setPlanDesarrollo(planDesarrollo);
		LOG.info("Municipio: \n{}", JsonUtil.toJsonString(municipio));
		kSession.insert(municipio);
	}
	
}

