package co.edu.uniandes.km.entrega3.perfilamiento;

import java.util.List;

public class EnfoquePlanDesarrollo {
	
	private String name;
	private List<Vertical> verticales;
	private String tipo;
	
	public EnfoquePlanDesarrollo(String name, List<Vertical> verticales, String tipo) {
		this.name = name;
		this.verticales = verticales;
		this.tipo = tipo;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the verticales
	 */
	public List<Vertical> getVerticales() {
		return verticales;
	}

	/**
	 * @param verticales the verticales to set
	 */
	public void setVerticales(List<Vertical> verticales) {
		this.verticales = verticales;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}