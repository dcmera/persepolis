package co.edu.uniandes.km.entrega3.bancoproyectos;

import java.util.ArrayList;
import java.util.List;

public class ServicioNegocio {

	private String nombre;
	private String solucion;
	private List<String> aplicaciones;	
	
	public ServicioNegocio(){
		this.nombre = "";
		this.solucion = "";
		this.aplicaciones = new ArrayList<>();
	}

	public ServicioNegocio(String nombre, String solucion, List<String> aplicaciones) {
		this.nombre = nombre;
		this.solucion = solucion;
		this.aplicaciones = aplicaciones;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the aplicaciones
	 */
	public List<String> getAplicaciones() {
		return aplicaciones;
	}

	/**
	 * @param aplicaciones the aplicaciones to set
	 */
	public void setAplicaciones(List<String> aplicaciones) {
		this.aplicaciones = aplicaciones;
	}
	
	/**
	 * @return the solucion
	 */
	public String getSolucion() {
		return solucion;
	}

	/**
	 * @param solucion the servicioNegocio to set
	 */
	public void setSolucion(String solucion) {
		this.solucion = solucion;
	}

	public boolean agregarAplicacion(String aplicacion){
		return this.aplicaciones.add(aplicacion);
	}
	
	public boolean removerAplicacion(String aplicacion){
		return this.aplicaciones.remove(aplicacion);
	}
}