package co.edu.uniandes.km.entrega3.bancoproyectos;

import java.util.ArrayList;
import java.util.List;

public class Solucion {
	
	private String nombre;
	private String iniciativa;
	private Double costo;
	private Double tiempoEjecucion;
	private Double indicadorEficiencia;
	private Double indicadorCumplimiento;
	private Double indicadorDesarrollo;
	private List<ServicioNegocio> serviciosNegocio;
	
	public Solucion(){
		this.nombre = "";
		this.iniciativa = "";
		this.costo = 0d;
		this.tiempoEjecucion = 0d;
		this.indicadorCumplimiento = 0d;
		this.indicadorDesarrollo = 0d;
		this.indicadorEficiencia = 0d;
		this.serviciosNegocio = new ArrayList<>();
	}

	public Solucion(String nombre, String iniciativa, Double costo, Double tiempoEjecucion, Double indicadorEficiencia,
			Double indicadorCumplimiento, Double indicadorDesarrollo, List<ServicioNegocio> serviciosNegocio) {
		this.nombre = nombre;
		this.iniciativa = iniciativa;
		this.costo = costo;
		this.tiempoEjecucion = tiempoEjecucion;
		this.indicadorEficiencia = indicadorEficiencia;
		this.indicadorCumplimiento = indicadorCumplimiento;
		this.indicadorDesarrollo = indicadorDesarrollo;
		this.serviciosNegocio = serviciosNegocio;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the costo
	 */
	public Double getCosto() {
		return costo;
	}

	/**
	 * @param costo the costo to set
	 */
	public void setCosto(Double costo) {
		this.costo = costo;
	}

	/**
	 * @return the tiempoEjecucion
	 */
	public Double getTiempoEjecucion() {
		return tiempoEjecucion;
	}

	/**
	 * @param tiempoEjecucion the tiempoEjecucion to set
	 */
	public void setTiempoEjecucion(Double tiempoEjecucion) {
		this.tiempoEjecucion = tiempoEjecucion;
	}

	/**
	 * @return the indicadorEficiencia
	 */
	public Double getIndicadorEficiencia() {
		return indicadorEficiencia;
	}

	/**
	 * @param indicadorEficiencia the indicadorEficiencia to set
	 */
	public void setIndicadorEficiencia(Double indicadorEficiencia) {
		this.indicadorEficiencia = indicadorEficiencia;
	}

	/**
	 * @return the indicadorCumplimiento
	 */
	public Double getIndicadorCumplimiento() {
		return indicadorCumplimiento;
	}

	/**
	 * @param indicadorCumplimiento the indicadorCumplimiento to set
	 */
	public void setIndicadorCumplimiento(Double indicadorCumplimiento) {
		this.indicadorCumplimiento = indicadorCumplimiento;
	}

	/**
	 * @return the indicadorDesarrollo
	 */
	public Double getIndicadorDesarrollo() {
		return indicadorDesarrollo;
	}

	/**
	 * @param indicadorDesarrollo the indicadorDesarrollo to set
	 */
	public void setIndicadorDesarrollo(Double indicadorDesarrollo) {
		this.indicadorDesarrollo = indicadorDesarrollo;
	}

	/**
	 * @return the serviciosNegocio
	 */
	public List<ServicioNegocio> getServiciosNegocio() {
		return serviciosNegocio;
	}

	/**
	 * @param serviciosNegocio the serviciosNegocio to set
	 */
	public void setServiciosNegocio(List<ServicioNegocio> serviciosNegocio) {
		this.serviciosNegocio = serviciosNegocio;
	}
	
	/**
	 * @return the iniciativa
	 */
	public String getIniciativa() {
		return iniciativa;
	}

	/**
	 * @param iniciativa the iniciativa to set
	 */
	public void setIniciativa(String iniciativa) {
		this.iniciativa = iniciativa;
	}

	public boolean agregarServicioNegocio(ServicioNegocio servicio){
		return this.serviciosNegocio.add(servicio);
	}
	
	public boolean removerServicioNegocio(ServicioNegocio servicio){
		return this.serviciosNegocio.remove(servicio);
	}

}
