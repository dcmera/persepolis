package co.edu.uniandes.km.entrega3;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public final class JsonUtil {

	private static final ObjectMapper MAPPER = new ObjectMapper();
	static{
		MAPPER.configure(SerializationFeature.INDENT_OUTPUT, true);
	}
	
	public static String toJsonString(Object object) throws JsonProcessingException{
		return MAPPER.writeValueAsString(object);
	}
}
