package co.edu.uniandes.km.entrega3.bancoproyectos;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;

public class BancoProyectos {

	private List<Iniciativa> iniciativas;
	
	public BancoProyectos(List<Iniciativa> iniciativas){
		this.iniciativas = iniciativas;
	}

	/**
	 * @return the iniciativas
	 */
	public List<Iniciativa> getIniciativas() {
		return iniciativas;
	}

	/**
	 * @param iniciativas the iniciativas to set
	 */
	public void setIniciativas(List<Iniciativa> iniciativas) {
		this.iniciativas = iniciativas;
	}
	
	public List<String> obtenerAreasPrioritarias(){
		List<String> areas = new ArrayList<>();
		iniciativas.forEach(i -> areas.addAll(i.getAreaPrioritarias()));
		return areas.stream().distinct().collect(Collectors.toList());
	}
	
	public void setIniciativasPorAreas(List<String> areasPrioritarias) throws JsonProcessingException{
		List<Iniciativa> toRemove = new ArrayList<>();
		for(Iniciativa i: iniciativas){
			for(String a: areasPrioritarias){
				if(!i.getAreaPrioritarias().contains(a)){
					toRemove.add(i);
				}
				else{
					break;
				}
			}
		}
		iniciativas.removeAll(toRemove);
		System.err.println("Despues de: " +iniciativas);
	}
}