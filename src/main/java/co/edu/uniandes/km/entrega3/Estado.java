package co.edu.uniandes.km.entrega3;

public enum Estado {

    SIN_VALIDACION,
    VALIDACION_CLASE,
    VALIDACION_AREA,
    VALIDACION_TIEMPO,
    VALIDACION_COSTO,
    VALIDACION_DESEMPENIO,
    PROPUESTA_GENERADA,
    FINAL,
    SALIR
}


