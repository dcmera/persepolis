package co.edu.uniandes.km.entrega3.perfilamiento;

public enum ClaseMunicipio{
	
	UNO,  //1
	DOS,	//2
	TRES,	//3
	CUATRO	//4
	;
	
	public static ClaseMunicipio fromInteger(int number){
		switch(number){
			case 1:
				return UNO;
			case 2:
				return DOS;
			case 3:
				return TRES;
			case 4:
				return CUATRO;
			default:
				throw new IllegalArgumentException("El numero " +  number + " no corresponde a ninguna clase");		
		}
	}
}
