package co.edu.uniandes.km.entrega3.bancoproyectos;

import java.util.List;

public class Iniciativa {
	
	private String nombre;
	private List<String> areaPrioritarias;
	private List<Solucion> soluciones;
	
	public Iniciativa(String nombre, List<String> areaPrioritaria, List<Solucion> soluciones) {
		this.nombre = nombre;
		this.areaPrioritarias = areaPrioritaria;
		this.soluciones = soluciones;
	}	
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the soluciones
	 */
	public List<Solucion> getSoluciones() {
		return soluciones;
	}

	/**
	 * @param soluciones the soluciones to set
	 */
	public void setSoluciones(List<Solucion> soluciones) {
		this.soluciones = soluciones;
	}	

	/**
	 * @return the areaPrioritaria
	 */
	public List<String> getAreaPrioritarias() {
		return areaPrioritarias;
	}

	/**
	 * @param areaPrioritaria the areaPrioritaria to set
	 */
	public void setAreaPrioritarias(List<String> areaPrioritaria) {
		this.areaPrioritarias = areaPrioritaria;
	}

	public boolean agregarSolucion(Solucion solucion){
		return this.soluciones.add(solucion);
	}
	
	public boolean removerSolucion(Solucion solucion){
		return this.soluciones.remove(solucion);
	}
}