package co.edu.uniandes.km.entrega2;

import java.text.NumberFormat;
import java.util.Locale;

public class Municipio {
	
	private String nombre;
	private Double presupuesto;
	private IndiceFinanciamiento indiceFinanciamiento;
	private PlanDesarrollo planDesarrollo;
	private CalificacionCrediticia calificacionCrediticia;
	
	public Municipio() {}

	public Municipio(String nombre, Double presupuesto, IndiceFinanciamiento indiceFinanciamiento,
			PlanDesarrollo planDesarrollo, CalificacionCrediticia calificacionCrediticia) {
		this.nombre = nombre;
		this.presupuesto = presupuesto;
		this.indiceFinanciamiento = indiceFinanciamiento;
		this.planDesarrollo = planDesarrollo;
		this.calificacionCrediticia = calificacionCrediticia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getPresupuesto() {
		return presupuesto;
	}

	public void setPresupuesto(Double presupuesto) {
		this.presupuesto = presupuesto;
	}

	public IndiceFinanciamiento getIndiceFinanciamiento() {
		return indiceFinanciamiento;
	}

	public void setIndiceFinanciamiento(IndiceFinanciamiento indiceFinanciamiento) {
		this.indiceFinanciamiento = indiceFinanciamiento;
	}

	public PlanDesarrollo getPlanDesarrollo() {
		return planDesarrollo;
	}

	public void setPlanDesarrollo(PlanDesarrollo planDesarrollo) {
		this.planDesarrollo = planDesarrollo;
	}

	public CalificacionCrediticia getCalificacionCrediticia() {
		return calificacionCrediticia;
	}

	public void setCalificacionCrediticia(CalificacionCrediticia calificacionCrediticia) {
		this.calificacionCrediticia = calificacionCrediticia;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		Locale colombia = new Locale.Builder().setLanguage("es").setRegion("CO").build();
		NumberFormat formato = NumberFormat.getCurrencyInstance(colombia);		
		StringBuilder builder = new StringBuilder();
		builder.append("Nombre Municipio: " + nombre + "\n");
		builder.append("Presupuesto: " + formato.format(presupuesto)  + "\n");
		builder.append("Calificacion crediticia: " + calificacionCrediticia.name() + "\n");
		builder.append(indiceFinanciamiento.toString() + "\n");
		builder.append(planDesarrollo.toString() + "\n");
		return builder.toString();
	}
}