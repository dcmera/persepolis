package co.edu.uniandes.km.entrega2;

public enum CalificacionCrediticia {
	NA,
	C,
	CC,
	CCC,
	B,
	BB,
	BBB,
	A,
	AA,
	AAA	
}