package co.edu.uniandes.km.entrega2;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Proyecto {

	private String nombre;
	private Double valor;
	private Map<Metrica, Double> metricas;
	private List<Problema> problemasResueltos;

	public Proyecto() {
		this.nombre = "";
		this.valor = (double) 0;
		this.metricas = new HashMap<>();
		this.problemasResueltos = new ArrayList<>();
	}

	public Proyecto(String nombre, Double valor, Map<Metrica, Double> caracteristicas,
			List<Problema> problemasResueltos) {
		this.nombre = nombre;
		this.valor = valor;
		this.metricas = caracteristicas;
		this.problemasResueltos = problemasResueltos;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the valor
	 */
	public Double getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(Double valor) {
		this.valor = valor;
	}

	/**
	 * @return the caracteristicas
	 */
	public Map<Metrica, Double> getMetricas() {
		return metricas;
	}

	/**
	 * @param caracteristicas
	 *            the caracteristicas to set
	 */
	public void setMetricas(Map<Metrica, Double> caracteristicas) {
		this.metricas = caracteristicas;
	}

	public List<Problema> getProblemasResueltos() {
		return problemasResueltos;
	}

	public void setProblemasResueltos(List<Problema> problemasResueltos) {
		this.problemasResueltos = problemasResueltos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		Locale colombia = new Locale.Builder().setLanguage("es").setRegion("CO").build();
		NumberFormat formato = NumberFormat.getCurrencyInstance(colombia);
		StringBuilder builder = new StringBuilder("[");
		builder.append("Proyecto: " + nombre + ", ");
		builder.append("Valor: " + formato.format(valor) + ", ");
		builder.append("Metricas: " + metricasToString() + ", ");
		builder.append("Problemas que resuelve: " + problemasToString() + " ");
		return builder.append("]").toString();
	}

	private String problemasToString() {

		if (problemasResueltos != null && !problemasResueltos.isEmpty()) {
			StringBuilder builder = new StringBuilder("[");
			problemasResueltos.forEach(problema -> builder.append(problema.name() + ", "));
			return builder.append("]").toString();
		}
		return "";

	}

	private String metricasToString() {

		if (metricas != null && !metricas.isEmpty()) {

			StringBuilder builder = new StringBuilder("[");
			for (Map.Entry<Metrica, Double> entry : metricas.entrySet()) {
				builder.append(String.format("- %s : %f ,", entry.getKey().name(), entry.getValue()));
			}
			return builder.append("]").toString();
		} else
			return "";
	}
}
