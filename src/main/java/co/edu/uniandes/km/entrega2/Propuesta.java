package co.edu.uniandes.km.entrega2;

import java.util.ArrayList;
import java.util.List;

public class Propuesta {

	List<Proyecto> proyectos;
	
	public Propuesta() {this.proyectos = new ArrayList<>();}
	
	public Propuesta(List<Proyecto> proyectos){
		this.proyectos = proyectos;
	}

	/**
	 * @return the proyectos
	 */
	public List<Proyecto> getProyectos() {
		return proyectos;
	}

	/**
	 * @param proyectos the proyectos to set
	 */
	public void setProyectos(List<Proyecto> proyectos) {
		this.proyectos = proyectos;
	}
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder("Proyectos: \n");
		proyectos.forEach(proyecto -> builder.append(proyecto.toString() + "\n"));
		return builder.toString();
	}
}