package co.edu.uniandes.km.entrega2;

public enum Problema {

	INSEGURIDAD,
	COBERTURA_SALUD,
	DESERCION_ESCOLAR,
	MORTALIDAD_INFANTIL,
	INFRAESTRUCTURA_VIAL,
	DESEMPLEO
}