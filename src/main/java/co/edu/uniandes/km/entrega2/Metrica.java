package co.edu.uniandes.km.entrega2;

public enum Metrica {

	EFICIENCIA,
	CUMPLIMIENTO,
	DESARROLLO,
	ADMON_PRESUPUESTO
}