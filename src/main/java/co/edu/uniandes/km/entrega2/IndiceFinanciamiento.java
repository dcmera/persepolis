package co.edu.uniandes.km.entrega2;

public class IndiceFinanciamiento {

	private Double solvencia;
	private Double sostenibilidad;
	private Double accesoCredito;
	private Double indice;
	
	public IndiceFinanciamiento() { }

	public IndiceFinanciamiento(Double solvencia, Double sostenibilidad, Double accesoCredito) {
		this.solvencia = solvencia;
		this.sostenibilidad = sostenibilidad;
		this.accesoCredito = accesoCredito;
		this.indice = (solvencia + sostenibilidad + accesoCredito)/3;
	}

	public Double getIndice() {
		return indice;
	}

	public Double getSolvencia() {
		return solvencia;
	}

	public void setSolvencia(Double solvencia) {
		this.solvencia = solvencia;
	}

	public Double getSostenibilidad() {
		return sostenibilidad;
	}

	public void setSostenibilidad(Double sostenibilidad) {
		this.sostenibilidad = sostenibilidad;
	}

	public Double getAccesoCredito() {
		return accesoCredito;
	}

	public void setAccesoCredito(Double accesoCredito) {
		this.accesoCredito = accesoCredito;
	}

	@Override
	public String toString() {
		return String.format("Indice de financiamiento: %f "
				+ "[Solvencia: %f Sostenibilidad: %f Acceso a Credito: %f]",
				indice, solvencia, sostenibilidad, accesoCredito);
	}
}
