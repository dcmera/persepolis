package co.edu.uniandes.km.entrega2;

import java.util.HashMap;
import java.util.Map;

public class PlanDesarrollo {

	private Map<Metrica, Double> indicadores;
	
	private Map<Problema, Integer> prioridarProblemas;
	
	public PlanDesarrollo(){
		this.indicadores = new HashMap<>();
		this.prioridarProblemas = new HashMap<>();
	}
	
	public PlanDesarrollo(Map<Metrica, Double> indicadores,  Map<Problema, Integer> prioridarProblemas) {
		this.setIndicadores(indicadores);
		this.prioridarProblemas = prioridarProblemas;
	}

	public Map<Metrica, Double> getIndicadores() {
		return indicadores;
	}

	public void setIndicadores(Map<Metrica, Double> indicadores) {
		this.indicadores = indicadores;
	}
	
	public Map<Problema, Integer> getPrioridarProblemas() {
		return prioridarProblemas;
	}

	public void setPrioridarProblemas(Map<Problema, Integer> prioridarProblemas) {
		this.prioridarProblemas = prioridarProblemas;
	}

	@Override
	public String toString(){
		String descripcion = "Plan Desarollo:\n";
		if(indicadores != null && !indicadores.isEmpty()){
			descripcion += "Indicadores del municipio:\n";
			for(Map.Entry<Metrica, Double> entry: indicadores.entrySet()){
				descripcion += " - " + entry.getKey().name() + ": " + entry.getValue() + "\n";
			}
		}
		else{
			descripcion += "No se especificaron indicadores\n";
		}
		if(prioridarProblemas != null && !prioridarProblemas.isEmpty()){
			descripcion += "Problemas del municipio:\n";
			for(Map.Entry<Problema, Integer> entry: prioridarProblemas.entrySet()){
				descripcion += " - " + entry.getKey().name() + ": " + entry.getValue() + "\n";
			}
		}
		else{
			descripcion += "No se conocen problemas para el municipio";
		}
		return descripcion;
	}
}