package co.edu.uniandes.km.entrega2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.kie.api.KieServices;
import org.kie.api.cdi.KSession;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * Clase principal de la aplicacion
 * @author Harry Wong, Francy Pineda, David Mera
 */
public class App {
	
	private App(){}

	public static void main(String[] args) {
		// Instrucciones del juego
		System.out.println("Esta aplicacion permite determinar si la propuesta es valida para el municipio");
				
		KieServices kService = KieServices.Factory.get();
		KieContainer kContainer = kService.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		agregarASesion(kSession);
		kSession.fireAllRules();
		kSession.dispose();
	}

	private static void agregarASesion(KieSession kSession) {
		Scanner scanner = new Scanner(System.in);
		/*
		agregarDatosMunicipio(kSession, scanner);
		agregarDatosPropuesta(kSession, scanner);
		*/

		IndiceFinanciamiento ifin = new IndiceFinanciamiento(0.6, 0.6, 0.6);
		Map<Metrica, Double> indicadores = new HashMap<>();
		indicadores.put(Metrica.CUMPLIMIENTO, (double) 0.6);
		indicadores.put(Metrica.EFICIENCIA, (double) 0.4);
		Map<Problema, Integer> problemasMunicipio = new HashMap<>();
		problemasMunicipio.put(Problema.COBERTURA_SALUD, 7);
		problemasMunicipio.put(Problema.DESEMPLEO, 6);
		PlanDesarrollo pd = new PlanDesarrollo(indicadores, problemasMunicipio);
		Municipio m = new Municipio("", (double)4500, ifin, pd, CalificacionCrediticia.BBB);
		
		List<Proyecto> proys = new ArrayList<>();
		
		Map<Metrica, Double> indicadoresPy1 = new HashMap<>();
		indicadoresPy1.put(Metrica.CUMPLIMIENTO, (double) 0.7);
		indicadoresPy1.put(Metrica.EFICIENCIA, (double) 0.6);
		proys.add(new Proyecto("Erradicar desercion escolar",
						(double)80, indicadoresPy1,
						Arrays.asList(Problema.DESERCION_ESCOLAR)));
		
		Map<Metrica, Double> indicadoresPy2 = new HashMap<>();
		indicadoresPy2.put(Metrica.CUMPLIMIENTO, (double) 0.5);
		indicadoresPy2.put(Metrica.EFICIENCIA, (double) 0.3);
		proys.add(new Proyecto("Erradicar desempleo",
				(double)80, indicadoresPy2,
				Arrays.asList(Problema.DESEMPLEO)));
		
		Propuesta p = new Propuesta(proys);		
		kSession.insert(m);
		kSession.insert(p);
		kSession.insert(new EtapaValidacion());
		//kSession.insert(new EtapaValidacion(Validacion.VALIDACION_POLITICA, true));
	}

	private static void agregarDatosMunicipio(KieSession kSession, Scanner scanner) {
		System.out.println("Digite a continuacion la informacion del municipio");
		System.out.print("Nombre: ");
		String nombre = scanner.nextLine();
		System.out.print("Presupuesto: ");
		Double presupuesto = scanner.nextDouble();
		
		System.out.println("A continuacion describa la capacidad financiera [0 a 1] del municipio: ");
		System.out.print("Solvencia: ");
		Double solvencia = scanner.nextDouble();
		System.out.print("Sostenibilidad: ");
		Double sostenibilidad = scanner.nextDouble();
		System.out.print("Acceso a creditos: ");
		Double accesoCredito = scanner.nextDouble();
		
		System.out.println("A continuacion describa los indicadores del plan de desarrollo.\n"
				+ "Cada uno de los indicadores debe representar el desempe�o del municipio en el rango de -1 a 1");
		Map<Metrica, Double> indicadores = new HashMap<>();
		for(Metrica metrica : Metrica.values()){
				System.out.print("Valor del Indicador " + metrica.name() + ": ");
				Double valor = scanner.nextDouble();
				indicadores.put(metrica, valor);
		}
		System.out.println("A continuacion describa la prioridad [0, 10] de los problemas del municipio");
		Map<Problema, Integer> prioridadProblemas = new HashMap<>();
		for(Problema problema : Problema.values()){
			System.out.print("Prioridad para " + problema.name() + ": ");
			Integer valor = scanner.nextInt();
			prioridadProblemas.put(problema, valor);
		}
		
		Municipio m = new Municipio(nombre, presupuesto,
				new IndiceFinanciamiento(solvencia, sostenibilidad, accesoCredito),
				new PlanDesarrollo(indicadores, prioridadProblemas),
				CalificacionCrediticia.NA);
		
		kSession.insert(m);
	}
	
	private static void agregarDatosPropuesta(KieSession kSession, Scanner scanner) {
			
		System.out.println("Ingrese la informacion de la propuesta: ");
		
		List<Proyecto> proyectos = new ArrayList<>();
		
		while(true){
			System.out.println("1. Agregar Proyecto.");
			System.out.println("2. Continuar.");
			System.out.print("Opcion: ");
			int opcion = scanner.nextInt();
			if(opcion == 2){
				break;
			}
			else if(opcion == 1){
				proyectos.add(agregarProyecto(scanner));
			}
		}
		if(proyectos.isEmpty()){
			System.out.println("La propuesta no tiene proyectos, el programa va a terminar");
			System.exit(0);
		}
		else{
			Propuesta propuesta = new Propuesta(proyectos);
			kSession.insert(propuesta);
		}
	}

	private static Proyecto agregarProyecto(Scanner scanner) {
		
		Proyecto proyecto = new Proyecto();
		System.out.print("Nombre del proyecto: ");
		proyecto.setNombre(scanner.next());
		System.out.print("Valor del proyecto: ");
		proyecto.setValor(scanner.nextDouble());
		Map<Metrica, Double> metricas = new HashMap<>();
		System.out.println("Metricas del proyecto: ");
		for(Metrica metrica : Metrica.values()){
			System.out.print("Valor de la metrica " + metrica.name() + ": ");
			Double valor = scanner.nextDouble();
			metricas.put(metrica, valor);
		}
		proyecto.setMetricas(metricas);
		System.out.println("Problemas que resuleve el proyecto: ");
		List<Problema> problemasResueltos = new ArrayList<>();
		for(Problema p: Problema.values()){
			System.out.print("�Resuelve " + p + "? ");
			boolean res = scanner.nextBoolean();
			if(res){
				problemasResueltos.add(p);
			}
		}		
		proyecto.setProblemasResueltos(problemasResueltos);
		return proyecto;		
	}
}