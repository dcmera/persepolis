package co.edu.uniandes.km.entrega2;

public class EtapaValidacion {

	private Validacion validacion;
	private boolean analizada;
	
	public EtapaValidacion(){
		this.validacion = Validacion.SIN_VALIDACION;
		this.analizada = false;
	}
	
	public EtapaValidacion(Validacion validacion, boolean analizada){
		this.validacion = validacion;
		this.analizada =  analizada;
	}

	/**
	 * @return the validacion
	 */
	public Validacion getValidacion() {
		return validacion;
	}

	/**
	 * @param validacion the validacion to set
	 */
	public void setValidacion(Validacion validacion) {
		this.validacion = validacion;
	}

	/**
	 * @return the analizada
	 */
	public boolean isAnalizada() {
		return analizada;
	}

	/**
	 * @param analizada the analizada to set
	 */
	public void setAnalizada(boolean analizada) {
		this.analizada = analizada;
	}
	
}